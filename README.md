# Islands Role Play Game #

Islands is a basic role play game with basic features

# Features #

  - Choose your own character
  - Choose an Island
  - Explore the Island
  - Fight with enemies and gain strength
  - Resume your game any time

# How it Works #
All maps have the same number of squares but they have different visible squares to explore. let's see an example below

| x | x | x  | x  | x  | x  |
|:--:|:--:|:--:|:--:|:--:|:--:|
| x  | x  | **9**  | **10** | x | x ||
| x | **14** | **15** | **16** | **17** | x ||
| x | **20** | **21** | **22** | **23** | x ||
| x | x | **27** | **28** | xx | x ||
| x | x | x | x | x | x ||

in last example we can see a map of 36 sqaures but only visible ``[9,10,14,...]`` so this is the positions where the player can explore

# Installation #
`` git clone git@bitbucket.org:abumostafa/islands.git``

# Tests #
``php vendor/bin/phpunit Tests/``
# Docs

in order to create a new Game you need to provide a Map and a GamePlayer objects

You need to create a Map or load it from your storage
```php
$map = new Map();
$map->setName('Pirates Island');
$map->setVisibleSquares([9, 10, 14, 15, 16, 17, 20, 21, 22, 23, 27, 28]);
$map->setStartPosition(9);
```

You need to create a User or load it from your storage
```php
$user = new User();
$user->setUsername('some-user-name');
```

You need to create a Character or load it from your storage
```php
$character = new Character();
$character->setName('Jonas "Grog Swiller" Blamey');
$character->setImage('https://image.ibb.co/gBKmVa/male.png');
```

You need to create a GamePlayer or load it from your storage
```php
$gamePlayer = new GamePlayer();
$gamePlayer->setUser($user);
$gamePlayer->setCharacter($character);
$gamePlayer->setPower(100);
$gamePlayer->setStrength(1);
```

### Create a new game
You have to provide a manager implemented from ``Islands\Contracts\GamePlayerManager``
```php
$manager = new Islands\Manager\PDO\UserGameManager($connection);
$gameService = new Islands\Service\GameService($manager);
$gameService->create($map, $player); // Now already created a new game
```

### Explore the map
to explore the map you need to visit an existing position
```php
$gameService->visit(22); // if position is not visible or doesn't exist an Exception will be thrown
```

### Fight an Enemy
To fight an enemy you have to add enemies to the game
On Fight
- The attacker will lose power and gain strength
- The defender will lose power and lose strength

You need to create an Enemy or load it from your storage
```php
$enemyCharacter = new Character();
$enemyCharacter->setName('Annie "Coconut Crazy" Dagger');
$enemyCharacter->setImage('https://image.ibb.co/myah3v/female.png');
```

create an Enemy
```php
$enemy = new Enemy();
$enemy->setName('Annie "Coconut Crazy" Dagger');
$enemy->setCharacter($enemyCharacter);
```

You need to create a GameEnemy or load it from your storage
```php
$gameEnemy = new GameEnemy();
$gameEnemy->setEnemy($enemy);
$gameEnemy->setPower(20);
$gameEnemy->setStrength(1);
```

Attack the enemy
```php
$gameService->attack($gameEnemy);
```

### Restore an Existing game
```php
$gameService->restoreById($id);
```
