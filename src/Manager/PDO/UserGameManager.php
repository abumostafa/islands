<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Doctrine\DBAL\Connection;
use Islands\Model\Game;
use Islands\Model\User;
use Islands\Contracts\UserGameManager as UserGameManagerContract;

/**
 * User Game Manager
 *
 * @package Islands\Manager\PDO
 */
class UserGameManager extends AbstractManager implements UserGameManagerContract
{
    /**
     * @var GamePlayerManager
     */
    protected $gamePlayerManager;

    /**
     * @var MapManager
     */
    protected $mapManager;

    /**
     * UserGameManager constructor.
     *
     * @param Connection $db
     * @param GamePlayerManager $gamePlayerManager
     * @param MapManager $mapManager
     */
    public function __construct(Connection $db, GamePlayerManager $gamePlayerManager, MapManager $mapManager)
    {
        parent::__construct($db);
        $this->gamePlayerManager = $gamePlayerManager;
        $this->mapManager = $mapManager;
    }

    /**
     * @inheritDoc
     */
    public function createGame(User $user, array $data)
    {
        try {
            $this->db->beginTransaction();

            $gamePlayer = $this->gamePlayerManager->createGamePlayer([
                'user_id' => $user->getId(),
                'character_id' => $data['character_id'],
            ]);

            $this->db->insert($this->getTableName(), [
                'map_id' => $data['map_id'],
                'player_id' => $gamePlayer->getId(),
            ]);

            $gameId = $this->db->lastInsertId();

            $this->db->commit();

            return $this->mapObject([
                'id' => $gameId,
                'map_id' => $data['map_id'],
                'player_id' => $gamePlayer->getId(),
            ]);
        } catch (\Exception $e) {

            if ($this->db->isTransactionActive()) {
                $this->db->rollBack();
            }

            throw $e;
        }
    }

    public function updateGame(Game $game)
    {
        $this->db->update($this->getTableName(), [
            'position' => $game->getPosition(),
        ], ['id' => $game->getId()]);

        return $game;
    }

    /**
     * @inheritDoc
     */
    protected function mapObject(array $data)
    {
        $model = parent::mapObject($data);

        if (!empty($data['map_id'])) {
            $model->setMap($this->mapManager->findOneOrFail(['id' => $data['map_id']]));
        }

        if (!empty($data['player_id'])) {
            $model->setPlayer($this->gamePlayerManager->findOneOrFail(['id' => $data['player_id']]));
        }

        return $model;
    }

    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'games';
    }

    /**
     * @inheritDoc
     */
    public function createModel()
    {
        return new Game();
    }
}