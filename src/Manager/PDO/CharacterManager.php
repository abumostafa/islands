<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Islands\Contracts\CharacterManager as CharacterManagerContract;
use Islands\Model\Character;

/**
 * Character Manager
 *
 * @package Islands\Manager\PDO
 */
class CharacterManager extends AbstractManager implements CharacterManagerContract
{
    /**
     * @inheritDoc
     */
    public function listCharacters($type)
    {
        return $this->db->fetchAll(sprintf('SELECT id, name, image FROM `%s` where `type` = ?', $this->getTableName()), [Character::TYPE_PLAYER]);
    }

    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'characters';
    }

    /**
     * @inheritDoc
     */
    public function createModel()
    {
        return new Character();
    }
}