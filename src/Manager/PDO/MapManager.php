<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Islands\Model\Map;
use Islands\Contracts\MapManager as MapManagerContract;

/**
 * Map Manager
 *
 * @package Islands\Manager\PDO
 */
class MapManager extends AbstractManager implements MapManagerContract
{
    /**
     * @inheritDoc
     */
    public function listMaps()
    {
        return $this->db->fetchAll(sprintf('SELECT id, name, visible_squares, start_position FROM `%s`', $this->getTableName()));
    }

    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'maps';
    }

    /**
     * @inheritDoc
     */
    public function createModel()
    {
        return new Map();
    }
}