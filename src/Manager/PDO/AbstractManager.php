<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Doctrine\DBAL\Connection;
use Islands\Exception\DatabaseObjectNotFoundException;

/**
 * Abstract Manager
 *
 * @package Islands\Manager\PDO
 */
abstract class AbstractManager
{
    /**
     * @var Connection
     */
    protected $db;

    /**
     * @return mixed
     */
    abstract protected function getTableName();

    /**
     * @return object
     */
    abstract public function createModel();

    /**
     * PlayerCharacterManager constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Find a record or fail
     *
     * @param array $criteria
     * @return object
     * @throws DatabaseObjectNotFoundException
     */
    public function findOneOrFail(array $criteria)
    {
        $wheres = str_repeat(', ?', count($criteria) - 1);
        $row = $this->db->fetchAssoc(sprintf('SELECT * FROM %s WHERE ? %s', $this->getTableName(), $wheres), array_values($criteria));

        if (!$row) {
            throw new DatabaseObjectNotFoundException(sprintf('unable to find row in table "%s" for criteria %s', $this->getTableName(), implode(', ', $criteria)));
        }

        return $this->mapObject($row);
    }

    /**
     * @param array $data
     * @return object
     */
    protected function mapObject(array $data)
    {
        $model = $this->createModel();

        foreach ($data as $column => $value) {
            $setter = ucwords(str_replace('_', ' ', $column));
            $setter = 'set' . str_replace(' ', '', $setter);

            if (method_exists($model, $setter)) {
                $model->$setter($value);
            }
        }

        return $model;
    }
}