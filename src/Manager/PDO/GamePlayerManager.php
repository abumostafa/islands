<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Islands\Contracts\GamePlayerManager as GamePlayerManagerContract;
use Doctrine\DBAL\Connection;
use Islands\Model\GamePlayer;

/**
 * Game Player Manager
 *
 * @package Islands\Manager\PDO
 */
class GamePlayerManager extends AbstractManager implements GamePlayerManagerContract
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var CharacterManager
     */
    protected $characterManager;

    /**
     * GamePlayerManager constructor.
     *
     * @param Connection $db
     * @param UserManager $userManager
     * @param CharacterManager $characterManager
     */
    public function __construct(Connection $db, UserManager $userManager, CharacterManager $characterManager)
    {
        parent::__construct($db);

        $this->userManager = $userManager;
        $this->characterManager = $characterManager;
    }

    /**
     * @inheritDoc
     */
    public function createGamePlayer(array $data)
    {
        $data = array_merge(['power' => 100, 'strength' => 1], $data); // add default power and strength for new player
        $this->db->insert($this->getTableName(), $data);

        return $this->mapObject([
            'id' => $this->db->lastInsertId(),
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function mapObject(array $data)
    {
        $model = parent::mapObject($data);

        if (!empty($data['user_id'])) {
            $model->setUser($this->userManager->findOneOrFail(['id' => $data['user_id']]));
        }

        if (!empty($data['character_id'])) {
            $model->setCharacter($this->characterManager->findOneOrFail(['id' => $data['character_id']]));
        }

        return $model;
    }

    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'game_players';
    }

    /**
     * @inheritDoc
     */
    public function createModel()
    {
        return new GamePlayer();
    }
}