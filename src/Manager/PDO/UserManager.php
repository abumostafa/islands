<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Manager\PDO
 */

namespace Islands\Manager\PDO;

use Islands\Model\User;
use Islands\Contracts\UserManager as UserManagerContract;

/**
 * User Manager
 *
 * @package Islands\Manager\PDO
 */
class UserManager extends AbstractManager implements UserManagerContract
{
    /**
     * @inheritDoc
     */
    public function createUser(User $user)
    {
        $this->db->insert($this->getTableName(), ['username' => $user->getUsername()]);

        $user->setId($this->db->lastInsertId());

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function createModel()
    {
        return new User();
    }

    /**
     * @inheritDoc
     */
    protected function getTableName()
    {
        return 'users';
    }
}