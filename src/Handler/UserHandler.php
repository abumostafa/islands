<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Handler
 */

namespace Islands\Handler;

use Islands\Contracts\UserManager;
use Islands\Exception\InvalidFormException;

/**
 * User Handler
 *
 * @package Islands\Handler
 */
class UserHandler
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * UserHandler constructor.
     *
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Handle Create User
     *
     * @param $data
     * @return mixed
     * @throws InvalidFormException
     */
    public function handleCreate($data)
    {
        return $this->userManager->createUser($data);
    }
}