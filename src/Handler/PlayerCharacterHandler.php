<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Handler
 */

namespace Islands\Handler;

use Islands\Contracts\CharacterManager;
use Islands\Model\Character;

/**
 * Player Character Handler
 *
 * @package Islands\Handler
 */
class PlayerCharacterHandler
{
    /**
     * @var CharacterManager
     */
    protected $characterManager;

    /**
     * PlayerCharacterHandler constructor.
     *
     * @param CharacterManager $characterManager
     */
    public function __construct(CharacterManager $characterManager)
    {
        $this->characterManager = $characterManager;
    }

    /**
     * Handle List Characters
     *
     * @return array
     */
    public function handleList()
    {
        return $this->characterManager->listCharacters(Character::TYPE_PLAYER);
    }
}