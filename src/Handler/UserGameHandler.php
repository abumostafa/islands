<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Handler
 */

namespace Islands\Handler;

use Islands\Contracts\UserGameManager;
use Islands\Contracts\UserManager;
use Islands\Model\Game;
use Islands\Service\GameService;

/**
 * User Game Handler
 *
 * @package Islands\Handler
 */
class UserGameHandler
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var GameService
     */
    protected $gameService;

    /**
     * UserGameHandler constructor.
     *
     * @param UserManager $userManager
     * @param GameService $gameService
     */
    public function __construct(UserManager $userManager, GameService $gameService)
    {
        $this->userManager = $userManager;
        $this->gameService = $gameService;
    }

    /**
     * Handle create user game
     *
     * @param int $userId
     * @param $data
     * @return Game
     */
    public function handleCreate($userId, $data)
    {
        $user = $this->userManager->findOneOrFail(['id' => $userId]);

        $this->gameService->create($user, $data);
        return $this->gameService->getGame();
    }

    /**
     * Handle update user game
     *
     * @param $gameId
     * @param $data
     * @internal param Game $game
     * @return Game
     */
    public function handleUpdate($gameId, $data)
    {
        return $this->gameService
            ->restoreById($gameId)
            ->visit($data['position'])
            ->getGame();
    }

    /**
     * Handle fetch user game
     *
     * @param $gameId
     * @return Game
     */
    public function handleFetch($gameId)
    {
        return $this->gameService
            ->restoreById($gameId)
            ->getGame();
    }
}