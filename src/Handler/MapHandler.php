<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Handler
 */

namespace Islands\Handler;

use Islands\Contracts\MapManager;

/**
 * Map Handler
 *
 * @package Islands\Handler
 */
class MapHandler
{
    /**
     * @var MapManager
     */
    protected $mapManager;

    /**
     * PlayerCharacterHandler constructor.
     *
     * @param MapManager $mapManager
     */
    public function __construct(MapManager $mapManager)
    {
        $this->mapManager = $mapManager;
    }

    /**
     * Handle List Maps
     *
     * @return array
     */
    public function handleList()
    {
        return $this->mapManager->listMaps();
    }
}