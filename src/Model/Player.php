<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           06/04/2017
 * @project        Islands
 * @package        Islands\Model
 */
namespace Islands\Model;

/**
 * Abstract Player Model
 *
 * @package Islands\Model
 */
abstract class Player
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var
     */
    protected $power;

    /**
     * @var
     */
    protected $strength;

    /**
     * @var Character
     */
    protected $character;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param mixed $power
     */
    public function setPower($power)
    {
        $this->power = $power;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param mixed $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return Character
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param Character $character
     */
    public function setCharacter($character)
    {
        $this->character = $character;
    }
}