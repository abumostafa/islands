<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           06/04/2017
 * @project        Islands
 * @package        Islands\Model
 */
namespace Islands\Model;

/**
 * GameEnemy Model
 *
 * @package Islands\Model
 */
class GameEnemy extends Player
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Enemy
     */
    protected $enemy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Enemy
     */
    public function getEnemy()
    {
        return $this->enemy;
    }

    /**
     * @param Enemy $enemy
     */
    public function setEnemy($enemy)
    {
        $this->enemy = $enemy;
    }
}