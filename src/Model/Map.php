<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           06/04/2017
 * @project        Islands
 * @package        Islands\Model
 */
namespace Islands\Model;

/**
 * Map Model
 *
 * @package Islands\Model
 */
class Map
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $visibleSquares;

    /**
     * @var int
     */
    protected $startPosition;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param array $visibleSquares
     */
    public function setVisibleSquares($visibleSquares)
    {
        $this->visibleSquares = $visibleSquares;
    }

    /**
     * @param int $startPosition
     */
    public function setStartPosition($startPosition)
    {
        $this->startPosition = $startPosition;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getVisibleSquares()
    {
        return json_decode($this->visibleSquares, true);
    }

    /**
     * @return int
     */
    public function getStartPosition()
    {
        return $this->startPosition;
    }
}