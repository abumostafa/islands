<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           06/04/2017
 * @project        Islands
 * @package        Islands\Model
 */
namespace Islands\Model;

/**
 * Game Model
 *
 * @package Islands\Model
 */
class Game
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Map
     */
    protected $map;

    /**
     * @var GamePlayer
     */
    protected $player;

    /**
     * @var
     */
    protected $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param Map $map
     */
    public function setMap($map)
    {
        $this->map = $map;
    }

    /**
     * @return Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position ? $this->position : $this->getMap()->getStartPosition();
    }

    /**
     * @return GamePlayer
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param GamePlayer $player
     */
    public function setPlayer(GamePlayer $player)
    {
        $this->player = $player;
    }
}