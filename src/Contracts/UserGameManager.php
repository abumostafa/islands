<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

use Islands\Model\Game;
use Islands\Model\User;

/**
 * User Game Manager Contract
 *
 * @package Islands\Contracts
 */
interface UserGameManager extends BaseManager
{
    /**
     * Create new game
     *
     * @param User $user
     * @param array $data
     * @return Game
     * @throws \Exception
     */
    public function createGame(User $user, array $data);

    /**
     * Update an existing game
     *
     * @param Game $game
     * @return Game
     * @throws \Exception
     */
    public function updateGame(Game $game);
}