<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

/**
 * Map Manager Contract
 *
 * @package Islands\Contracts
 */
interface MapManager extends BaseManager
{
    /**
     * @return array
     */
    public function listMaps();
}