<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

use Islands\Model\User;

/**
 * User Manager Contract
 *
 * @method User findOneOrFail(array $criteria)
 * @package Islands\Contracts
 */
interface UserManager extends BaseManager
{
    /**
     * @param User $user
     * @return User
     */
    public function createUser(User $user);
}