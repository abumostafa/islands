<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

/**
 * Character Manager Contract
 *
 * @package Islands\Contracts
 */
interface CharacterManager extends BaseManager
{
    /**
     * @param $type
     * @return array
     */
    public function listCharacters($type);
}