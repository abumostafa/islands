<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

use Islands\Model\GamePlayer;

/**
 * Game Player Manager Contract
 *
 * @package Islands\Contracts
 */
interface GamePlayerManager extends BaseManager
{
    /**
     * Crate game player
     *
     * @param array $data
     * @return GamePlayer
     */
    public function createGamePlayer(array $data);
}