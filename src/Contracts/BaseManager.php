<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Contracts
 */

namespace Islands\Contracts;

use Islands\Exception\DatabaseObjectNotFoundException;


/**
 * Interface BaseManager
 *
 * @package Islands\Contracts
 */
interface BaseManager
{
    /**
     * Find a row by id or throw an exception
     *
     * @param array $criteria
     * @throws DatabaseObjectNotFoundException
     * @return object
     */
    public function findOneOrFail(array $criteria);
}