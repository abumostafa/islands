<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           06/04/2017
 * @project        Islands
 * @package        Islands\Service
 */

namespace Islands\Service;

use Islands\Contracts\UserGameManager;
use Islands\Exception\UnexpectedValueException;
use Islands\Model\Game;
use Islands\Model\GameEnemy;
use Islands\Model\Player;
use Islands\Model\User;

/**
 * Game Service for Handling game operations
 *
 * @package Islands\Service
 */
class GameService
{
    /**
     * @var Game
     */
    protected $game;

    /**
     * @var UserGameManager
     */
    private $gameManager;

    /**
     * GameService constructor.
     * @param UserGameManager $gameManager
     */
    public function __construct(UserGameManager $gameManager)
    {
        $this->gameManager = $gameManager;
    }

    /**
     * Create new Game
     *
     * @param User $user
     * @param array $data
     *
     * @return GameService
     */
    public function create(User $user, array $data)
    {
        $this->game = $this->gameManager->createGame($user, $data);

        return $this;
    }

    /**
     * @param $id
     * @return $this
     * @throws EntityNotFoundException
     */
    public function restoreById($id)
    {
        $this->game = $this->gameManager->findOneOrFail(['id' => $id]);

        return $this;
    }

    /**
     * Explore a new Square
     *
     * @param $square
     * @return $this
     * @throws \Exception
     */
    public function visit($square)
    {
        $map = $this->getGame()->getMap();

        if (!in_array($square, $map->getVisibleSquares())) {
            throw new UnexpectedValueException('Square is not available');
        }

        $this->getGame()->setPosition($square);

        $this->gameManager->updateGame($this->getGame());

        return $this;
    }

    /**
     * Attack an Enemy
     *
     * @param GameEnemy $enemy
     * @return $this
     */
    public function attack(GameEnemy $enemy)
    {
        $this->onAttack($this->getGame()->getPlayer(), $enemy);

        $this->gameManager->updateGame($this->getGame());

        return $this;
    }

    /**
     * Calculate new Power and Strength for Attacker and Defender
     *
     * @param Player $attacker
     * @param Player $defender
     * @return $this
     * @throws \Exception
     */
    protected function onAttack(Player $attacker, Player $defender)
    {
        $attacker->setPower($attacker->getPower() - 1);// attacker will lose strength
        $attacker->setStrength($attacker->getStrength() + 1); // attacker will gain strength

        $defender->setPower($defender->getPower() - 2);// defender will lose strength
        $defender->setStrength($defender->getStrength() - 1);// defender will lose power

        return $this;
    }

    /**
     * @return Game
     * @throws \Exception
     */
    public function getGame()
    {
        if (!$this->game) {
            throw new \Exception('Please create a game first');
        }

        return $this->game;
    }
}