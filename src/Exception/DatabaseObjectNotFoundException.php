<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Exception
 */

namespace Islands\Exception;

/**
 * Database Object Not Found Exception
 *
 * @package Islands\Exception
 */
class DatabaseObjectNotFoundException extends \Exception
{

}