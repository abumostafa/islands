<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           09/04/2017
 * @project        Islands
 * @package        Islands\Exception
 */

namespace Islands\Exception;

/**
 * InvalidFormException
 *
 * @package Islands\Exception
 */
class InvalidFormException extends \Exception
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * InvalidFormException constructor.
     *
     * @param array $errors
     * @param string $message
     * @param int $code
     * @param \Exception|null $prev
     */
    public function __construct(array $errors, $message = 'Validation Failed', $code = 400, \Exception $prev = null)
    {
        $this->errors = $errors;

        parent::__construct($message, $code, $prev);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}