<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           07/04/2017
 * @project        Islands
 * @package        Islands\Exception
 */
namespace Islands\Exception;

class UnexpectedValueException extends \UnexpectedValueException
{

}