<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @date           07/04/2017
 * @project        Islands
 * @package        Islands\Tests\Functional\GameService
 */

namespace Islands\Tests\Functional\GameService;

use Islands\Contracts\GamePlayerManager;
use Islands\Contracts\MapManager;
use Islands\Contracts\UserGameManager;
use Islands\Exception\UnexpectedValueException;
use Islands\Service\GameService;
use Islands\Model\Character;
use Islands\Model\Enemy;
use Islands\Model\Game;
use Islands\Model\GameEnemy;
use Islands\Model\GamePlayer;
use Islands\Model\Map;
use Islands\Model\User;
use PHPUnit\Framework\TestCase;

class GameServiceTest extends TestCase
{
    public function testCreateNewGame()
    {
        $storageProps = $this->getStorageData();
        $user = $this->createUser($storageProps['user']);

        $game = $this->createGameService($this->getGameManager())
            ->create($user, ['character_id' => 1, 'map_id' => 1])
            ->getGame();

        $this->assertInstanceOf(Game::class, $game);

        $this->assertEquals($storageProps['map']['name'], $game->getMap()->getName());
        $this->assertEquals($storageProps['map']['visible_squares'], $game->getMap()->getVisibleSquares());
        $this->assertEquals($storageProps['map']['start_position'], $game->getMap()->getStartPosition());

        $this->assertEquals($storageProps['player_character']['name'], $game->getPlayer()->getCharacter()->getName());
        $this->assertEquals($storageProps['player_character']['image'], $game->getPlayer()->getCharacter()->getImage());

        $this->assertEquals($storageProps['user']['username'], $game->getPlayer()->getUser()->getUsername());

        $this->assertEquals($storageProps['game_player']['power'], $game->getPlayer()->getPower());
        $this->assertEquals($storageProps['game_player']['strength'], $game->getPlayer()->getStrength());

        $this->assertEquals($storageProps['map']['start_position'], $game->getPosition());
    }

    public function testAttack()
    {
        $props = $this->getStorageData();
        $enemy = $this->createEnemyModel(array_merge(['name' => $props['enemy']['name']], ['character' => $this->createCharacter($props['enemy_character'])]));

        $gameEnemy = $this->createGameEnemyModel(array_merge(['enemy' => $enemy,], $props['game_enemy']));

        $this->assertEquals($props['game_enemy']['power'], $gameEnemy->getPower());
        $this->assertEquals($props['game_enemy']['strength'], $gameEnemy->getStrength());

        $game = $this->createGameService($this->getGameManager())
            ->restoreById(1)
            ->attack($gameEnemy)
            ->getGame();

        $this->assertEquals(99, $game->getPlayer()->getPower());
        $this->assertEquals(2, $game->getPlayer()->getStrength());

        $this->assertEquals(48, $gameEnemy->getPower());
        $this->assertEquals(0, $gameEnemy->getStrength());
    }

    public function testVisit()
    {
        $game = $this->createGameService($this->getGameManager())
            ->restoreById(1)
            ->visit(16)
            ->getGame();

        $this->assertEquals(16, $game->getPosition());
    }

    public function testVisitNonExistingPosition()
    {
        $this->expectException(UnexpectedValueException::class);

        $this->createGameService($this->getGameManager())
            ->restoreById(1)
            ->visit(33);
    }

    public function testRestoreGame()
    {
        $gameService = $this->createGameService($this->getGameManager())->restoreById(1);
        $this->assertInstanceOf(Game::class, $gameService->getGame());
        $this->assertInstanceOf(Map::class, $gameService->getGame()->getMap());
        $this->assertInstanceOf(GamePlayer::class, $gameService->getGame()->getPlayer());
    }

    protected function getStorageData()
    {
        return [
            'map' => ['name' => 'Sylt', 'visible_squares' => [9, 10, 14, 15, 16, 17, 20, 21, 22, 23, 27, 28], 'start_position' => 9],
            'user' => ['username' => 'john.smith',],
            'player_character' => ['name' => 'Gordon "Captain Of Rats" Rye', 'image' => 'https://image.ibb.co/gBKmVa/male.png'],
            'game_player' => ['power' => 100, 'strength' => 1],
            'enemy_character' => ['name' => 'Annie "Coconut Crazy" Dagger', 'image' => 'https://image.ibb.co/myah3v/female.png'],
            'enemy' => ['name' => 'Enemy 1'],
            'game_enemy' => ['power' => 50, 'strength' => 1],
            'game' => ['position' => null],
        ];
    }

    /**
     * @param UserGameManager $gameManager
     * @param Game $game
     * @return GameService
     */
    protected function createGameService(UserGameManager $gameManager)
    {
        return new GameService($gameManager);
    }

    protected function createEmptyStorage()
    {
        $storage = $this->getMockBuilder(StorageContract::class)->getMock();
        $storage->expects($this->any())->method('findGameBy')->willReturn(null);
        return $storage;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createGameModel(array $props)
    {
        $game = new Game();
        $game->setMap($props['map']);
        $game->setPlayer($props['player']);
        $game->setPosition($props['position']);

        return $game;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createMapModel(array $props)
    {
        $map = new Map();
        $map->setName($props['name']);
        $map->setVisibleSquares($props['visible_squares']);
        $map->setStartPosition($props['start_position']);
        return $map;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createGamePlayerModel(array $props)
    {
        $gamePlayer = new GamePlayer();
        $gamePlayer->setUser($props['user']);
        $gamePlayer->setCharacter($props['character']);
        $gamePlayer->setPower($props['power']);
        $gamePlayer->setStrength($props['strength']);
        return $gamePlayer;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createUser(array $props)
    {
        $user = new User();
        $user->setUsername($props['username']);
        return $user;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createCharacter(array $props)
    {
        $character = new Character();
        $character->setName($props['name']);
        $character->setImage($props['image']);
        return $character;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createGameEnemyModel(array $props)
    {
        $gameEnemy = new GameEnemy();
        $gameEnemy->setEnemy($props['enemy']);
        $gameEnemy->setPower($props['power']);
        $gameEnemy->setStrength($props['strength']);
        return $gameEnemy;
    }

    /**
     * @param array $props
     * @return Game
     */
    protected function createEnemyModel(array $props)
    {
        $enemy = new Enemy();
        $enemy->setName($props['name']);
        $enemy->setCharacter($props['character']);
        return $enemy;
    }

    /**
     * @return MapManager
     */
    protected function getMapManager()
    {
        $mapData = $this->getStorageData()['map'];
        $manager = $this->getMockBuilder(MapManager::class)->getMock();
        $manager->expects($this->any())->method('findOneOrFail')->willReturn($this->createMapModel($mapData));

        return $manager;
    }

    /**
     * @return GamePlayerManager
     */
    protected function getGamePlayerManager()
    {
        $gamePlayerData = $this->getStorageData()['game_player'];

        $user = $this->createUser([
            'username' => $props['user']['username']
        ]);

        $playerCharacter = $this->createCharacter([
            'name' => $props['player_character']['name'],
            'image' => $props['player_character']['image'],
        ]);

        $gamePlayer = $this->createGamePlayerModel([
            'user' => $user,
            'character' => $playerCharacter,
            'power' => $gamePlayerData['power'],
            'strength' => $gamePlayerData['strength'],
        ]);

        $manager = $this->getMockBuilder(GamePlayerManager::class)->getMock();
        $manager->expects($this->any())->method('findOneOrFail')->willReturn($gamePlayer);

        return $manager;
    }

    protected function getGameManager()
    {
        $storageProps = $this->getStorageData();

        $map = $this->createMapModel($storageProps['map']);
        $user = $this->createUser($storageProps['user']);
        $character = $this->createCharacter($storageProps['player_character']);
        $player = $this->createGamePlayerModel(array_merge(['user' => $user, 'character' => $character], $storageProps['game_player']));
        $game = $this->createGameModel(array_merge(['map' => $map, 'player' => $player], ['position' => null]));

        $manager = $this->getMockBuilder(UserGameManager::class)->getMock();
        $manager->expects($this->any())->method('createGame')->willReturn($game);
        $manager->expects($this->any())->method('findOneOrFail')->willReturn($game);

        return $manager;
    }

    protected function getEmptyGameManager()
    {
        $manager = $this->getMockBuilder(UserGameManager::class)->getMock();
        $manager->expects($this->any())->method('findOneOrFail')->willReturn(null);

        return $manager;
    }
}